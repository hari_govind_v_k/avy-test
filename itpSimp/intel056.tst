c RUN: bash -c "%avy %OPT2 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %itp-simp-minisat %p/$(basename %s tst)aig" | OutputCheck %REM %s
c CHECK: ^BRUNCH_STAT Result UNSAT$
instance from hwmcc17
expect convergence in 15 frames 
c RUN: bash -c "%avy %OPT${$1} %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT""$1"" %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTm %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTabs %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTkind %p/$(basename %s tst)aig" | OutputCheck %REM %s



c RUN: bash -c "%avy %OPTQUIP1 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP2 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP3 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP4 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP5 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP6 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP7 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP8 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP9 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIPabs %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIPkind %p/$(basename %s tst)aig" | OutputCheck %REM %s
