c RUN: bash -c "%avy %OPT2 %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %itp-simp-minisat %p/$(basename %s tst)aig" | OutputCheck %REM %s
c CHECK: ^BRUNCH_STAT Result UNSAT$
verilog code for the counter circuit 
module kind (
  input clk,
  output  x,
  output  a,
  output  b,
  output  c
);
  reg  x = 1;
  reg  a = 1;
  reg  b = 1;
  reg  c = 1;

  always @(posedge clk) begin
    x=a|b|c;
    a=b|a;
    b=c;
    c=0;
  end

  assert property (x == 1 );
endmodule
c RUN: bash -c "%avy %OPT${$1} %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPT""$1"" %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTm %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTabs %p/$(basename %s tst)aig" | OutputCheck %REM %s
c RUN: bash -c "%avy %OPTkind %p/$(basename %s tst)aig" | OutputCheck %REM %s



c RUN: bash -c "%avy %OPTQUIP1 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP2 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP3 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP4 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP5 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP6 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP7 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP8 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIP9 %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIPabs %p/$(basename %s tst)aig" | OutputCheck %REM %s

c RUN: bash -c "%avy %OPTQUIPkind %p/$(basename %s tst)aig" | OutputCheck %REM %s
